export class Usuario{

    nombre: string;
    email: string;
    password: string;

    public clonar(): Usuario{
        let clon = new Usuario();
        clon.nombre = this.nombre;
        clon.email = this.email;
        clon.password = this.password;

        return clon;
    }
}