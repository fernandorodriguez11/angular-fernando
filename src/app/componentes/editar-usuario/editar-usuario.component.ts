import { Component, OnInit, Input } from '@angular/core';
import { AlmacenUsuariosService } from 'src/app/servicios/almacen-usuarios.service';
import { Usuario } from 'src/app/entidades/Usuario';

@Component({
  selector: 'app-editar-usuario',
  templateUrl: './editar-usuario.component.html',
  styleUrls: ['./editar-usuario.component.css']
})
export class EditarUsuarioComponent{

  campoNombre: string = "Nombre: ";
  campoPassword: string = "Password: ";
  campoEmail: string = "Email: ";

  //con el @input, hacemos que el usuario venga del componente padre
  @Input() usuarioEdit: Usuario;
  @Input() editando: Boolean;

  public getClassCss(){
    if(this.editando){
      return "inputs-editar";
    }else{
      return "inputs";
    }
  }

  public getClassCssCapa(){
    if(this.editando){
      return "camposFormularios-editar";
    }else{
      return "camposFormularios"
    }
  }

}
