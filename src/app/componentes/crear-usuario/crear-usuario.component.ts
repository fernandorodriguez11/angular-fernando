import { Component } from "@angular/core";
import { Usuario } from '../../entidades/Usuario';
import { AlmacenUsuariosService } from '../../servicios/almacen-usuarios.service';

@Component({
    //Caracteristicas (metadatos)
    selector: 'app-crear-usuario',
    templateUrl: `crear-usuario.component.html`, //Template si escribimos html en el componente y 
                                                //TemplateUrl si creamos un .html
    styleUrls: ['crear-usuario.component.css']
})
export class CrearUsuarioComponent{
    //datos del UI y su funcionalidad (Modelo del componente)
    subTitulo: string = "Registro de Usuario";
    cajita: Boolean;
    usuExiste: String;

    usuarioNuevo: Usuario;//Creo una variable que va a ser de tipo usuario
    srvUsuarios: AlmacenUsuariosService;// Declaramos

    //Al crear el constructor instanciamos el objeto.
    //Se hace en el constructor porque lo dice angular. Angular se encarga de instanciar
    //el servicio(hacer el new Al..).
    // Y aquí reconoce que lo necesitamos, por el tipo del parámetro.
    constructor(srvUsu: AlmacenUsuariosService){
        this.usuarioNuevo = new Usuario();
        this.usuarioNuevo.nombre = "";
        this.usuarioNuevo.email = "";
        this.usuarioNuevo.password = "";
        this.srvUsuarios = srvUsu; //Almacenar la referencia.
        
        this.cajita = false;
        this.usuExiste = "";
    }


    //Creamos el evento para cuando pulsemos click en el botón haga algo
    registrarAlpulsarClick(){
        if(this.usuarioNuevo.nombre === "" || this.usuarioNuevo.email === "" || 
        this.usuarioNuevo.password === ""){
            this.usuExiste = "* Los campos no pueden estar vacíos";
            return false;
        }else{
            if(this.srvUsuarios.insertar(this.usuarioNuevo)){
                this.cajita = true;
            }else{
                this.usuExiste = "* Error este usuario ya existe";
            }
            this.usuarioNuevo = new Usuario()
        }

    }

}