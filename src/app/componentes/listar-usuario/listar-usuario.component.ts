import { Component } from "@angular/core";
import { AlmacenUsuariosService } from '../../servicios/almacen-usuarios.service';
import { Usuario } from 'src/app/entidades/Usuario';

@Component({
    //Caracteristicas (metadatos)
    selector: 'app-listar-usuario',
    templateUrl: 'listar-usuario.component.html',
    styleUrls: ['./listar-usuario.component.css']
})
export class ListarUsuarioComponent{
    campo1: string = "Nombre";
    campo2: string = "Apellidos";
    campo3: string = "Password";

    posicion: number;
    usuario: Usuario;

    muestraMensaje:Boolean;
    muestraEditar:Boolean;
    cssEditar : Boolean;

    listaUsuarios: Usuario[];

    constructor(private srvUsu: AlmacenUsuariosService){
        this.muestraMensaje = false;
        this.muestraEditar = false;
        this.cssEditar = false;
    }

    listadoDatos(){
       return this.listaUsuarios = this.srvUsu.listLocalStrg();
    }

    /**
     * Compruebo que me muestra el formulario para editar usuario que yo he seleccionado.
     * @param posicionE identificador del usuario seleccionado
     * @param i 
     * @param usuario para pasar los datos del usuario al formulario
     */
    lanzarEditar(posicionE, i, usuario: Usuario){
        if(posicionE == i){
            this.muestraEditar = true;
            this.usuario = usuario;
            this.cssEditar = true;
        }
    }

    /**Función encargada de modificar un usuario en la local storage y una vez editado
     * se oculta el formulario de editar. En el array se modifica debido a que estamos
     * editando una referencia de objeto.
     */
    editar(){
        this.srvUsu.guardarLocalStrg();
        this.muestraEditar = false;
    }

    cancelarEditar(){
        this.listaUsuarios = this.listadoDatos();
    }

    /*Método que hace que me aparezca una ventana emergente donde el usuario decide si borrar o no*/
    lanzarBorrar(posicion,i, usuario: Usuario){
        
        if(posicion == i){
            this.muestraMensaje = true;
            this.posicion = posicion;
            this.usuario = usuario;
        }
        
    }

    //Función encargada de eliminar un usuario del array pasando la posicion de ese elemento.
    borrar(posicionObjeto: number, usuario: Usuario){
        
        if(this.srvUsu.eliminarDeLaLocalStrg(posicionObjeto)){
            this.muestraMensaje = false;
            alert(`Usuario ${usuario.nombre} con mail ${usuario.email} ha sido eliminado correctamente`);
        }else{
            this.muestraMensaje = false;
            alert(`Usuario ${usuario.nombre} con mail ${usuario.email} no ha sido eliminado correctamente`);
        }
        
    }

}