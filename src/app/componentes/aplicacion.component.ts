import { Component } from "@angular/core";
import { AlmacenUsuariosService } from '../servicios/almacen-usuarios.service';

@Component({
    //Caracteristicas (metadatos)
    selector: 'app-componet-raiz',
    template: `<router-outlet></router-outlet>`
})
export class AplicacionComponent{
    //datos del UI y su funcionalidad (Modelo del componente)
    srvUsuario: AlmacenUsuariosService;

    constructor(srvUsu: AlmacenUsuariosService){
        this.srvUsuario = srvUsu;
    }
/*<h1 *ngFor="let n of cambiaNombre()">{{ n.nombre }}</h1>
    cambiaNombre(){
       return this.srvUsuario.changeName();
    }*/
}