import { Injectable } from '@angular/core';
import { Usuario } from '../entidades/Usuario';

@Injectable({
  providedIn: 'root'
})
export class AlmacenUsuariosService {

  private listaUsuarios: Usuario[];
  //private nombreUsuario: Usuario[];

  constructor() {
    this.listaUsuarios = [];
    //this.nombreUsuario = [];
    
    this.cargaLocalStrg();
    if(this.listaUsuarios == null || typeof this.listaUsuarios === "undefined"){
      this.listaUsuarios =[];
    }
  }

  //Pasamos el objeto usuario con los datos que cogemos del formulario.
  //si el usuario es insertado correctamente en el array retornamos true para mostrar
  //un mensaje de usuario insertado correctamente
  insertar(usuario: Usuario ){
  
    let existeEmail = false;

    for( let usu of this.listaUsuarios){

      if(usu.email === usuario.email){
        existeEmail = true;
        break;
      }
    }

    if(existeEmail){
      //alert(`El email ${usuario.email} ya existe en la base de datos.`);
      return false;
    }else{
      this.addToArray(usuario);
      return true;
    }

  }

  //Vamos a guardar el objeto en la local storage del navegador. El navegador solo admite
  //textos numeros y booleans, por eso el array de objeto hay que convertirlo en json y 
  // posteriormente en texto.
  guardarLocalStrg(){

    let textoJsonUsuario = JSON.stringify(this.listaUsuarios);

    //Con window.localStorage accedemos a la local storage
    //Con set item guardamos en la cache el listado de objetos en tipo string añadiendole un nombre
    window.localStorage.setItem("listaUsuarios", textoJsonUsuario);

  }

  eliminarDeLaLocalStrg(posicion: number){
    
    this.listaUsuarios.splice(posicion,1);
    this.guardarLocalStrg()
    return true;
    
  }

  addToArray(user: Usuario){
    //Ejemplo clonado
    //let clonado = usuario.clonar();
    //this.listaUsuarios.push(clonado);
    this.listaUsuarios.push(user);
    this.guardarLocalStrg();
  }

  cargaLocalStrg(){
    let obtenDatosLStr = window.localStorage.getItem("listaUsuarios");

     this.listaUsuarios = JSON.parse(obtenDatosLStr);
  }
  listLocalStrg(){

    return this.listaUsuarios;

  }

}
