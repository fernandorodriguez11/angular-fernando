import { NgModule } from "@angular/core";
import { FormsModule} from '@angular/forms';
import { BrowserModule } from "@angular/platform-browser";
import { RouterModule, Routes} from "@angular/router"

import { AplicacionComponent } from "./componentes/aplicacion.component"; //Importamos el TS. Importamos la clase del typeScript
import { CrearUsuarioComponent } from "./componentes/crear-usuario/crear-usuario.component"
import { ListarUsuarioComponent } from './componentes/listar-usuario/listar-usuario.component';
import { EditarUsuarioComponent } from './componentes/editar-usuario/editar-usuario.component';
import { MenuComponent } from './componentes/menu/menu.component';
import { ErrorComponent } from './componentes/error/error.component';

const rutasApp :Routes =[
    {path:"menu",
    component:MenuComponent},
    {path:"crear",
    component:CrearUsuarioComponent},
    {path:"lista",
    component:ListarUsuarioComponent},
    {path:"",
    redirectTo:"/menu",
    pathMatch:"full"},
    {path:"**",
    component:ErrorComponent}
    
   ]

//Un módulo es una clase. Esta clase espera una clase AppModule porque es el que pide el main

//Especificamos que es un modulo con la annotation NgModule
@NgModule({
    declarations: [AplicacionComponent,CrearUsuarioComponent, ListarUsuarioComponent, EditarUsuarioComponent, MenuComponent, ErrorComponent],//Los componentes se declaran
    imports: [BrowserModule,FormsModule,RouterModule.forRoot(rutasApp,{enableTracing:true})], //Solo se importan otros módulos
    bootstrap: [AplicacionComponent]
})
export class AppModule{
    //puede ser una clase vacía.
}